from fossology import Fossology, fossology_token
from fossology.obj import TokenScope

import random
import string

def generate_random_string(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

CI_PROJECT_NAME = "graphic_lite"
CI_COMMIT_SHA = generate_random_string(10)

FOSSOLOGY_SERVER = "https://fossology.openharmony.testingmachine.eu/repo"
FOSSOLOGY_USER = "fossy"
FOSSOLOGY_PASSWORD = "fossy"
TOKEN_NAME = "GitLab CI " + CI_COMMIT_SHA

token = fossology_token(
    FOSSOLOGY_SERVER,
    FOSSOLOGY_USER,
    FOSSOLOGY_PASSWORD,
    TOKEN_NAME,
    TokenScope.WRITE
)

foss = Fossology(
    FOSSOLOGY_SERVER,
    token,
    FOSSOLOGY_USER
)

uploads = foss.list_uploads()

print(uploads)
